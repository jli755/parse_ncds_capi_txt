Parse tagged NCDS CAPI text file.

Usage: Put your tageed NCSD CAPI text file into tagged_input directory.  Delete old ones, it should only contain one file.

Input:
  - tagged text file, with label at the end of each line to indicate if this is a question/instruction etc
    - response domain
      - format: Label,Type,Type2,Format,Min,Max
      - example: Generic Text,Text,,,,20	response domain
    - condition
      - extra line on condition label
      - logic inside curly brackets {logic}
    - loop
      - inside curly brackets {Label, Variable, Start_Value, End_Value}

Output:
  - parsed dir: 
    - seperate input file into different types
  - db_input dir: 
    - combine files from parsed dir


Next step:
  - run [archivist_insert_pipeline](https://gitlab.com/closer-cohorts1/archivist_insert) with files from db_input directory

