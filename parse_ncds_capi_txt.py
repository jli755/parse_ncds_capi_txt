#!/bin/env python3

"""
    Parse cleaned up NCDS CAPI txt file
    - no order needed for condition/loop
    - only one parent type: section

    At tagging stage, please
    - codelist: put one category in one line,
                for example
                5 a barracks, nurses' hall of residence, or other accommodation provided by your	code list
                employer	code list
                should become
                5 a barracks, nurses' hall of residence, or other accommodation provided by your employer	code list
    - response domain: what format should we use to include all information?
    - no instruction inside statement

    Note on nested conditions:
    - check how many | at the beginning of the line, otherwise two ifs will be lumped into one
"""

import pandas as pd
import numpy as np
import os
import re


def parse_txt(txt_file, output_dir):
    """
    parse text file into seperate tables
    """

    sequence_file = os.path.join(output_dir, 'sequence.csv')
    statement_label_file = os.path.join(output_dir, 'statement_label.csv')
    statement_literal_file = os.path.join(output_dir, 'statement_literal.csv')
    instruction_file = os.path.join(output_dir, 'instruction.csv')
    question_label_file = os.path.join(output_dir, 'question_label.csv')
    question_literal_file = os.path.join(output_dir, 'question_literal.csv')
    codelist_file = os.path.join(output_dir, 'codelist.csv')
    response_file = os.path.join(output_dir, 'response.csv')
    condition_label_file = os.path.join(output_dir, 'condition_label.csv')
    condition_file = os.path.join(output_dir, 'condition.csv')
    loop_file = os.path.join(output_dir, 'loop.csv')

    with open(txt_file) as in_file, \
         open(sequence_file, 'w+') as out_sequence, \
         open(statement_label_file, 'w+') as out_statement_label, \
         open(statement_literal_file, 'w+') as out_statement_literal, \
         open(instruction_file, 'w+') as out_instruction, \
         open(question_label_file, 'w+') as out_question_label, \
         open(question_literal_file, 'w+') as out_question_literal, \
         open(codelist_file, 'w+') as out_codelist, \
         open(response_file, 'w+') as out_response, \
         open(condition_label_file, 'w+') as out_condition_label, \
         open(condition_file, 'w+') as out_condition, \
         open(loop_file, 'w+') as out_loop:

        out_sequence.write('Position\tLabel\n')
        out_statement_label.write('Label\tParent_Name\tPosition\tLineNumber\n')
        out_statement_literal.write('Label\tLiteral\n')
        out_instruction.write('Label\tInstructions\n')
        out_question_label.write('Label\tParent_Name\tPosition\tLineNumber\n')
        out_question_literal.write('Label\tLiteral\n')
        out_codelist.write('QuestionLabel\tCodelist\n')
        out_response.write('QuestionLabel\tLabel\tType\tType2\tFormat\tMin\tMax\n')
        out_condition_label.write('Label\tParent_Name\tPosition\tLineNumber\n')
        out_condition.write('Label\tLiteral\tParent_Name\tPosition\tLineNumber\n')
        out_loop.write('oldLabel\tLiteral\tParent_Name\tPosition\tLineNumber\n')

        sequence_num = 0
        item_num = 0

        prev_line = None
        prev_label = ''
        prevBar = 0

        for n, line in enumerate(in_file):
            # check how many '|' in this line
            numBar = line.count('|', 0, len(line))

            # remove | from beginging of the line
            line = re.sub("^(\| +)*", "", line)
            # print(line)
            L = line.split("\t")
            # print(L)
            assert len(L) == 2, f'Line {n+1} "{line}" has two many tab chars'
            content, key = L
            key = key.rstrip().lower()

            # print(f"Line {n + 1}: key={key}\tcontent={content}")

            if key == 'statement':
                if prev_key == "question label":
                    raise ValueError("TODO: hack around question_label with statement label")
                allowed_prev_keys = ("statement", "statement label", "instruction")
                if prev_key not in allowed_prev_keys:
                    msg = f'Line {n + 1}: We have a "{key}" '
                    msg += f'but the previous key "{prev_key}"\nis not in the '
                    msg += f'allowed set:\n  {allowed_prev_keys}\n'
                    msg += f'The lines are:\n>>>  {prev_line}>>>  {line}Perhaps content is not of type "{key}"?'
                    raise ValueError(msg)
                out_statement_literal.write("{}\t{}\n".format(current_label, content))

            elif key == 'question literal':
                allowed_prev_keys = ("question literal", "question label", "instruction")
                if prev_key not in allowed_prev_keys:
                    msg = f'We have a "{key}" '
                    msg += f'but previous key "{prev_key}"\nis not in the '
                    msg += f'allowed set:\n  {allowed_prev_keys}\n'
                    msg += f'Perhaps content\n>>>  {content}\nis not of type "{key}"?'
                    # print(msg)
                    raise ValueError("see above")
                out_question_literal.write("{}\t{}\n".format(current_label, content))

            elif key == "sequence":
                sequence_num = sequence_num + 1
                current_sequence = content
                item_num = 0
                out_sequence.write('%d\t%s\n' %(sequence_num, content))

            elif key == "statement label":
                item_num += 1
                out_statement_label.write("{}\t{}\t{}\t{}\n".format('s_' + content.strip(), current_sequence, item_num, n))
                current_label = 's_' + content.strip()

            elif key == "question label":
                item_num += 1
                out_question_label.write("{}\t{}\t{}\t{}\n".format('qi_' + content.strip(), current_sequence, item_num, n))
                current_label = 'qi_' + content.strip()

            elif key == "instruction":
                out_instruction.write(f"{current_label}\t{content}\n")

            elif key == "response domain":
                out_response.write('%s\t%s\n' %(current_label, content.strip().replace(',', '\t')))

            elif key == "code list":
                out_codelist.write(f"{current_label}\t{content}\n")

            elif key == "condition label":
                item_num += 1
                out_condition_label.write("{}\t{}\t{}\t{}\n".format(content, current_sequence, item_num, n))
                current_label = content

            elif key == "condition text":
                out_condition.write(f"{current_label}\t{content}\t{current_sequence}\t{item_num}\t{n}\n")

            elif key == "loop":
                item_num += 1
                out_loop.write(f"{current_label}\t{content}\t{current_sequence}\t{item_num}\t{n}\n")
            else:
                # print(line)
                # print(prev_line)
                raise NotImplementedError(f"XXXXX key={key} not implemented")
            prev_line = line
            prev_key = key
            prev_numBar = numBar


def get_db_input(study_name, input_dir, output_dir):
    """
    merge tables from input_dir
    """

    df_question_label = pd.read_csv(os.path.join(input_dir, 'question_label.csv'), sep='\t')
    df_question_literal = pd.read_csv(os.path.join(input_dir, 'question_literal.csv'), sep='\t')
    df_statement_label = pd.read_csv(os.path.join(input_dir, 'statement_label.csv'), sep='\t')
    df_statement_literal = pd.read_csv(os.path.join(input_dir, 'statement_literal.csv'), sep='\t')
    df_instruction = pd.read_csv(os.path.join(input_dir, 'instruction.csv'), sep='\t')
    df_condition_label = pd.read_csv(os.path.join(input_dir, 'condition_label.csv'), sep='\t')
    df_condition_literal = pd.read_csv(os.path.join(input_dir, 'condition.csv'), sep='\t')
    df_codelist = pd.read_csv(os.path.join(input_dir, 'codelist.csv'), sep='\t')
    df_response = pd.read_csv(os.path.join(input_dir, 'response.csv'), sep='\t')
    df_loop = pd.read_csv(os.path.join(input_dir, 'loop.csv'), sep='\t')
    df_sequence = pd.read_csv(os.path.join(input_dir, 'sequence.csv'), sep='\t')

    # sequence
    sequence_columns = ['Label', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df_sequence['Parent_Name'] = study_name
    df_sequence['Parent_Type'] = 'CcSequence'
    df_sequence['Branch'] = None
    df_sequence_keep = df_sequence[sequence_columns].drop_duplicates()

    # insert a row
    df_sequence_keep.loc[-1] = [study_name, 'CcSequence', None, None, 1]
    # shifting index
    df_sequence_keep.index = df_sequence_keep.index + 1
    df_sequence_keep.sort_index(inplace=True)
    df_sequence_keep.to_csv(os.path.join(output_dir, 'sequence.csv'), sep='\t', index=False)

    # statement
    # merge statement literal
    df_statement_literal_m = df_statement_literal.groupby(['Label'])['Literal'].apply(','.join).reset_index()
    df_statement = df_statement_label.merge(df_statement_literal_m, on='Label', how='left')
    df_statement['Parent_Type'] = 'CcSequence'
    df_statement['Branch'] = None

    statement_columns = ['Label', 'Literal', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df_statement_keep = df_statement[statement_columns].drop_duplicates()
    df_statement_keep.to_csv(os.path.join(output_dir, 'statement.csv'), sep='\t', index=False)

    # question
    # merge question literal per Label
    df_question_literal_m = df_question_literal.groupby(['Label'])['Literal'].apply(','.join).reset_index()
    # merge instruction
    df_instruction_m = df_instruction.groupby(['Label'])['Instructions'].apply(','.join).reset_index()

    df_q = df_question_label.merge(df_question_literal_m, on='Label', how='left')
    df_question = df_q.merge(df_instruction_m, on='Label', how='left')

    # codelist
    df_codelist['Label'] = 'cs_' + df_codelist['QuestionLabel']
    df_codelist['Response'] = df_codelist['Label']
    df_codelist['ResponseType'] = 'codelist'

    thepattern = re.compile("(\(*\d+\)*)\ (.+)")
    df_codelist['pattern'] = df_codelist['Codelist'].apply(lambda x: thepattern.findall(x))
    # df_codelist.to_csv('tmp_codelist.csv', sep='\t', index=False)
    df_codelist['Code_Value'] = df_codelist['pattern'].apply(lambda x: x[0][0])
    df_codelist['Category'] = df_codelist['pattern'].apply(lambda x: x[0][1])
    df_codelist['Code_Order'] = df_codelist.groupby('Label').cumcount() + 1

    codelist_columns = ['Label', 'Code_Order', 'Code_Value', 'Category']
    df_codelist_keep = df_codelist[codelist_columns].drop_duplicates()
    df_codelist_keep.to_csv(os.path.join(output_dir, 'codelist.csv'), sep='\t', index=False)

    # response domain
    df_response['ResponseType'] = 'response'
    df_response['Response'] = df_response['Label']
    response_columns = ['Label', 'Type', 'Type2', 'Format', 'Min', 'Max']
    df_response_keep = df_response[response_columns].drop_duplicates()

    # remove duplicates
    df_response_out = df_response_keep.drop_duplicates(subset='Label', keep='first')
    # df_response_out.to_csv('tmp_response.csv', sep='\t', index=False)

    # change type
    df_response_out['Min'] = df_response_out['Min'].astype(float)
    df_response_out['Max'] = df_response_out['Max'].astype(float)
    df_response_out.to_csv(os.path.join(output_dir, 'response.csv'), sep='\t', index=False)

    # put response into question item
    df_question_response = df_question[['Label', 'LineNumber']].merge(df_response[['QuestionLabel', 'Response', 'ResponseType']], left_on = 'Label', right_on = 'QuestionLabel', how='inner')
    df_question_response = df_question_response.drop_duplicates()

    df_question_codelist = df_question[['Label', 'LineNumber']].merge(df_codelist[['QuestionLabel', 'Response', 'ResponseType']], left_on = 'Label', right_on = 'QuestionLabel', how='inner')
    df_question_codelist = df_question_codelist.drop_duplicates()

    # concat response and codelist, check if there are multiple answers
    df_question_r = pd.concat([df_question_response, df_question_codelist], ignore_index=True, sort=False)
    df_question_r = df_question_r.sort_values(['LineNumber', 'ResponseType'], ascending=[True, False])
    df_question_r.to_csv('TMP.csv')
    df_question_r = df_question_r.drop(['QuestionLabel', 'ResponseType', 'LineNumber'], axis=1)
    df_question_r = df_question_r.drop_duplicates()

    df_question_all = df_question.merge(df_question_r, on='Label', how='left')

    df_question_all['Parent_Type'] = 'CcSequence'
    df_question_all['Branch'] = None
    df_question_all['min_responses'] = 1
    df_question_all['max_responses'] = df_question_all.groupby(['Label'])['Response'].transform('count')

    df_question_all['rd_order'] = df_question_all.groupby('Label').cumcount() + 1

    df_question_all['Interviewee'] = 'Cohort/sample member'

    question_columns = ['Label', 'Literal', 'Instructions', 'Response', 'Parent_Type', 'Parent_Name', 'Branch', 'Position', 'min_responses', 'max_responses', 'rd_order', 'Interviewee']
    df_question_item = df_question_all[question_columns]
    # column type is integer
    df_question_item[['Position', 'Branch', 'rd_order']] = df_question_item[['Position', 'Branch', 'rd_order']].astype('Int64')

    df_question_item.to_csv(os.path.join(output_dir, 'question_item.csv'), sep='\t', index=False)

    # condition
    # merge condition literal
    df_condition_literal_m = df_condition_literal.groupby(['Label'])['Literal'].apply(','.join).reset_index()
    df_condition = df_condition_label.merge(df_condition_literal_m, on='Label', how='left')

    # find logic inside {}
    logic_pattern = re.compile(".*\{(.+?)\}.*")
    df_condition['pattern'] = df_condition['Literal'].apply(lambda x: logic_pattern.findall(x))
    df_condition['Logic'] = df_condition['pattern'].apply(lambda x: x[0] if len(x) != 0 else '')

    # remove {Logic} from literal
    df_condition['Literal'] = df_condition.apply(lambda row: row['Literal'].replace(row['Logic'], '').replace('{', '').replace('}', ''), axis=1)

    df_condition['Parent_Type'] = 'CcSequence'
    df_condition['Branch'] = None
    condition_columns = ['Label', 'Literal', 'Logic', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df_condition_keep = df_condition[condition_columns]
    df_condition_keep.to_csv(os.path.join(output_dir, 'condition.csv'), sep='\t', index=False)


    # loop
    loop_columns = ['Label', 'Loop_While', 'Start_Value', 'End_Value', 'Variable', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    # merge loop literal
    df_loop_m = df_loop.groupby(['oldLabel', 'Position', 'Parent_Name'])['Literal'].apply(','.join).reset_index()

    # find {label, variable, start value}
    loop_pattern = re.compile(".*\{(.+?)\}.*")
    df_loop_m['pattern'] = df_loop_m['Literal'].apply(lambda x: loop_pattern.findall(x))
    df_loop_m['text'] = df_loop_m['pattern'].apply(lambda x: x[0].strip() if len(x) != 0 else '')
    # split string into multiple columns
    df_loop_m[['Label', 'Variable', 'Start_Value', 'End_Value']] = df_loop_m['text'].str.split(',', expand=True)

    df_loop_m['Loop_While'] = df_loop_m['Literal'].apply(lambda x: x.split('{')[0])
    df_loop_m.Start_Value = df_loop_m.Start_Value.str.strip()
    # 'Start_Value', 'End_Value' should be integer
    df_loop_m[['Start_Value']] = df_loop_m[['Start_Value']].astype('float').astype('Int64')

    df_loop_m['Parent_Type'] = 'CcSequence'
    df_loop_m['Branch'] = None
    df_loop_keep = df_loop_m[loop_columns]

    df_loop_keep = df_loop_keep.sort_values(['Parent_Name', 'Position'], ascending=[True, True])

    df_loop_keep.to_csv(os.path.join(output_dir, 'loop.csv'), sep='\t', index=False)


def main():
    base_dir = 'tagged_input'
    input_name = [f for f in os.listdir(base_dir) if f.endswith('.txt')]
    input_txt = os.path.join(base_dir, input_name[0])

    parsed_dir = 'parsed'
    if not os.path.exists(parsed_dir):
        os.makedirs(parsed_dir)

    parse_txt(input_txt, parsed_dir)
    print('Extracted text file')

    db_input_dir = 'db_input_dir'
    if not os.path.exists(db_input_dir):
        os.makedirs(db_input_dir)

    study_name = 'NCDS'
    get_db_input(study_name, parsed_dir, db_input_dir)


if __name__ == "__main__":
   main()

